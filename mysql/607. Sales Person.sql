SELECT name
FROM SalesPerson
WHERE sales_id
NOT IN (
    SELECT O.sales_id
    FROM Orders AS O
    INNER JOIN Company AS C
    ON C.com_id = O.com_id
    WHERE C.name = 'RED'
);

-- Official solution
/*
SELECT
    s.name
FROM
    salesperson s
WHERE
    s.sales_id NOT IN (SELECT
            o.sales_id
        FROM
            orders o
                LEFT JOIN
            company c ON o.com_id = c.com_id
        WHERE
            c.name = 'RED')
;
*/