SELECT class
FROM Courses
GROUP BY class HAVING COUNT(student) >= 5;

-- Official solution (the same)
/*
SELECT
    class
FROM
    courses
GROUP BY class
HAVING COUNT(student) >= 5
;
*/