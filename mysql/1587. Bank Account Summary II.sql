SELECT name, SUM(amount) AS BALANCE
FROM Users AS U
INNER JOIN Transactions AS T
ON U.account = T.account
GROUP BY U.account
HAVING SUM(amount) > 10000
;