SELECT P.firstName, P.lastName, A.city, A.state
FROM Person AS P
LEFT OUTER JOIN Address AS A
ON P.PersonId = A.PersonId;