SELECT
    patient_id,
    patient_name,
    conditions
FROM
    Patients
WHERE
    conditions REGEXP '^DIAB1.*|\\sDIAB1.*'
;

-- Upvoted solution (了解\b在regex意義，\w與其他非\w的銜接處)
/*
SELECT * FROM patients WHERE conditions REGEXP '\\bDIAB1'
*/