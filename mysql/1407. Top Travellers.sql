SELECT U.name, SUM(IF(R.distance, R.distance, 0)) AS travelled_distance
FROM Users AS U
LEFT OUTER JOIN Rides AS R
ON U.id = R.user_id
GROUP BY U.id
ORDER BY travelled_distance DESC, U.name ASC