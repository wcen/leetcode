SELECT *
FROM Cinema
WHERE (id % 2) != 0
AND description != 'boring'
ORDER BY rating DESC;

-- Official solution
/*
select *
from cinema
where mod(id, 2) = 1 and description != 'boring'
order by rating DESC
;
*/