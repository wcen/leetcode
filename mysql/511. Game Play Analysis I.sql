SELECT player_id, MIN(event_date) AS first_login
FROM Activity
GROUP BY player_id;

-- Official solution
/*
SELECT
  A.player_id,
  MIN(A.event_date) AS first_login
FROM
  Activity A
GROUP BY
  A.player_id;
*/