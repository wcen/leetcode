SELECT C.name AS Customers
FROM Customers AS C
LEFT OUTER JOIN Orders AS O
ON O.CustomerId = C.id 
WHERE O.id IS NULL;

-- Official solution
/* 
select customers.name as 'Customers'
from customers
where customers.id not in
(
    select customerid from orders
);
*/