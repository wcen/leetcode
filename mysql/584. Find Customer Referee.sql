SELECT name
FROM Customer
WHERE referee_id != 2 OR referee_id IS NULL;

-- Official solution
/*
SELECT name FROM customer WHERE referee_id <> 2 OR referee_id IS NULL;
*/