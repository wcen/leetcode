SELECT id AS Id
FROM Weather AS W
WHERE temperature > (
    SELECT temperature
    FROM Weather
    WHERE recordDate = DATE_SUB(W.recordDate, INTERVAL 1 DAY)
);

-- Official solution
/*
SELECT
    weather.id AS 'Id'
FROM
    weather
        JOIN
    weather w ON DATEDIFF(weather.recordDate, w.recordDate) = 1
        AND weather.Temperature > w.Temperature
;
*/