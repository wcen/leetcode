SELECT P.product_id, P.product_name
FROM Product AS P
INNER JOIN Sales AS S
ON S.product_id = P.product_id
GROUP BY S.product_id
HAVING MIN(S.sale_date) >= '2019-01-01'
AND MAX(S.sale_date) <= '2019-03-31';

/*
SELECT product_id, sale_date
FROM Sales
GROUP BY product_id
HAVING MIN(sale_date) >= '2019-01-01' AND MAX(sale_date) <= '2019-03-31';
*/