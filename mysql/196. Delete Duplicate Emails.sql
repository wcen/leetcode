DELETE P1
FROM Person AS P1
INNER JOIN Person AS P2
ON P1.email = P2.email
WHERE P1.id > P2.id

-- Official solution
/*
DELETE p1 FROM Person p1,
    Person p2
WHERE
    p1.Email = p2.Email AND p1.Id > p2.Id
*/