SELECT name, population, area
FROM World
WHERE area >= 3000000 OR population >= 25000000;

-- Official solution (the same but faster a little bit)
/*
SELECT
    name, population, area
FROM
    world
WHERE
    area >= 3000000

UNION

SELECT
    name, population, area
FROM
    world
WHERE
    population >= 25000000
;
*/