SELECT name AS Employee
FROM Employee AS A
WHERE salary > (
    SELECT salary
    FROM Employee
    WHERE id = A.managerId
);