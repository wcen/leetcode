"""
Q: 兩個ListNode合併為一個。
Link: https://leetcode.com/problems/merge-two-sorted-lists/
"""
from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        # 假設一個ListNode為頭
        dummy_node = ListNode()
        previous_node = dummy_node

        while list1 or list2:
            # 其中一個ListNode為None，則直接指向另一個ListNode
            if not list1:
                previous_node.next = list2
                break
            if not list2:
                previous_node.next = list1
                break

            if list1.val >= list2.val:
                previous_node.next = list2
                list2 = list2.next
            else:
                previous_node.next = list1
                list1 = list1.next

            previous_node = previous_node.next

        return dummy_node.next
