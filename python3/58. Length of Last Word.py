#!/usr/bin/env python3
"""
Q: 給一段字串句子，求出其最後一個單字的長度。
Link: https://leetcode.com/problems/length-of-last-word/
"""

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        str_list = s.split(' ')

        for _str in reversed(str_list):
            if _str:
                return len(_str)
        return 0

if __name__ == '__main__':
    print(Solution().lengthOfLastWord('I am happy '))
