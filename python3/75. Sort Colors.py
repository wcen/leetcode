"""
Q: 由小到大排順序，使用氣泡排序。
Link: https://leetcode.com/problems/sort-colors/
"""

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        curr_max_idx = len(nums) - 1    # 用來標示當下要排的最大數的位置（已經排序過的最大數，就不再遍歷一次）

        idx = 0
        while curr_max_idx > 0:
            if nums[idx] > nums[idx+1]:
                nums[idx], nums[idx+1] = nums[idx+1], nums[idx] # 數字較大的放後面（交換位置）

            if idx+1 == curr_max_idx:   # 已經檢查到最大數的位置
                curr_max_idx -= 1       # 更新下次要排的最大數的位置
                idx = 0                 # 從頭開始檢查
                continue
            else:
                idx += 1
