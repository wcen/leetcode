#!/usr/bin/env python3
"""
Q: 給一串羅馬數字字串，將其返回為整數。
Link: https://leetcode.com/problems/roman-to-integer/
"""

class Solution:
    def romanToInAns(self, s: str) -> int:
        """
        羅馬數字規則，index越大表示其代表的數值越小，若發現index小且數值也小，就表示是要減去的值。
        """
        map = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
        sList = list(s)
        nList = sList
        for i in range(0, len(sList)):
            nList[i] = map[sList[i]]
        for i in range(1, len(nList)):
            if nList[i] > nList[i-1]:
                nList[i] = nList[i] - nList[i-1]
                nList[i-1] = 0
        return sum(nList)

    def romanToInt(self, s: str) -> int:
        roman_mapping = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000,
        }
        s_list = list(s)
        int_list = []

        for idx, item in enumerate(s_list):
            int_list.append(roman_mapping.get(item))

            if item in ('V', 'X'):
                count = 1
                while count < 5:
                    pre_idx = idx - count
                    if pre_idx >= 0 and s_list[pre_idx] == 'I':
                        int_list[pre_idx] = -int_list[pre_idx]
                        count += 1
                    else:
                        break

            elif item in ('L', 'C'):
                count = 1
                while count < 5:
                    pre_idx = idx - count
                    if pre_idx >= 0 and s_list[pre_idx] == 'X':
                        int_list[pre_idx] = -int_list[pre_idx]
                        count += 1
                    else:
                        break

            #elif item in ['D', 'M']:
            elif item in ('D', 'M'):
                count = 1
                while count < 5:
                    pre_idx = idx - count
                    if pre_idx >= 0 and s_list[pre_idx] == 'C':
                        int_list[pre_idx] = -int_list[pre_idx]
                        count += 1
                    else:
                        break

        return sum(int_list)

if __name__ == '__main__':
    print(Solution().romanToInt('MMMCDXC'))
