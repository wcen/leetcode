"""
Q: 給定一陣列，其內容均為整數，且為升冪排序，再給定一目標變數，找出目標變數的插入位置（須符合升冪排序）。
Link: https://leetcode.com/problems/search-insert-position/
"""

from typing import List

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        for idx, num in enumerate(nums):
            if target > num:
                continue
            else:
                return idx

        return len(nums)
