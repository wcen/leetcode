"""
Q: 找出最長的前綴字串。
Link: https://leetcode.com/problems/longest-common-prefix/
"""
from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        # 最長的前綴字串，一定也在短的字串中，所以直接拿最短的字串去搜尋
        prefix = min(strs)
        prefix_len = len(prefix)

        for string in strs:
            while not string.startswith(prefix):
                prefix_len -= 1
                prefix = prefix[0:prefix_len]

        return prefix
