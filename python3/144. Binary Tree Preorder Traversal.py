"""
Q: 二元樹前序遍歷。
Link: https://leetcode.com/problems/binary-tree-preorder-traversal/
"""
from typing import Optional, List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def preorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        self.preorder(root, stack)
        return stack

    def preorder(self, node: Optional[TreeNode], stack: list):
        if node:
            stack.append(node.val)
            self.preorder(node.left, stack)
            self.preorder(node.right, stack)
        else:
            return
