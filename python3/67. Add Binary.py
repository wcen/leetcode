#!/usr/bin/env python3
"""
Q: 二進位相加。
Link: https://leetcode.com/problems/add-binary/
"""


class Solution:
    def addBinary(self, a: str, b: str) -> str:
        return bin(
            self.binToDec(a) + self.binToDec(b)
        )[2:]

    def binToDec(self, bin_str: str) -> int:
        dec_sum = 0
        for idx in range(0, len(list(bin_str))):
            dec_sum += int(bin_str[idx:idx+1]) * 2 ** (len(bin_str) - idx -1)

        return dec_sum


if __name__ == '__main__':
    print(Solution().addBinary('1010', '1011'))
