#!/usr/bin/env python3
"""
Q: 給定一整數陣列，求出其最大的子陣列總和。
Link: https://leetcode.com/problems/maximum-subarray/
"""
from typing import List


class Solution:
    # 暴力法，O(n^2)
    def nativeMaxSubArraySum(self, nums: List[int]) -> int:
        """ 全部都加過一遍，找出最大的和
        0   1   2   3   4   5
        +---+---+---+---+---+
        | h | e | l | l | o |
        +---+---+---+---+---+
        -5  -4  -3  -2  -1  0
        """
        for pointer1 in range(0, len(nums) + 1):
            for pointer2 in range(pointer1 + 1, len(nums) + 1):
                tmp_sum = sum(nums[pointer1:pointer2])

                if not 'max_sum' in locals() or tmp_sum > max_sum:
                    max_sum = tmp_sum

        return max_sum

    # Divide and Conquer, O(nLogn)
    def divideMaxSubArraySum(self, nums: List[int]) -> int:
        """ 切兩半，各取最大值；再考慮中間橫跨這兩半的部分
        """
        if len(nums) == 1:
            return nums[0]

        mid = len(nums) // 2
        left_half = nums[0:mid]
        right_half = nums[mid:len(nums)+1]

        max_left_sum = self.nativeMaxSubArraySum(left_half)
        max_right_sum = self.nativeMaxSubArraySum(right_half)
        max_cross_sum = self.maxCrossSubArraySum(left_half, right_half)

        return max(max_left_sum, max_right_sum, max_cross_sum)

    def maxCrossSubArraySum(
        self,
        leftArray: List[int],
        rightArray: List[int]
    ) -> int:
        leftArray.reverse()

        for idx in range(0, len(leftArray)):
            if not 'tmp_sum' in locals():
                tmp_sum = left_sum = leftArray[idx]
                continue

            tmp_sum += leftArray[idx]

            if tmp_sum > left_sum:
                left_sum = tmp_sum

        del tmp_sum

        for idx in range(0, len(rightArray)):
            if not 'tmp_sum' in locals():
                tmp_sum = right_sum = rightArray[idx]
                continue

            tmp_sum += rightArray[idx]

            if tmp_sum > right_sum:
                right_sum = tmp_sum

        return left_sum + right_sum

    # Kadane’s Algorithm, O(n)
    def kadaneMaxSubArraySum(self, nums: List[int]):
        """ 幾個要點：
        - 兩個變數，當前總和(tmp_sum)、最大總和(max_sum)
        - 只有正數會讓最大總和(max_sum)增加
        - 遇到當前總和(tmp_sum)為負值，表示要重新計算了，清為0
        - 取得當前總和(tmp_sum)後，先和最大總和(max_sum)比較
        """
        tmp_sum, max_sum = 0, float('-inf')
        for idx in range(0, len(nums)):
            tmp_sum += nums[idx]

            if tmp_sum > max_sum:
                max_sum = tmp_sum

            if tmp_sum < 0:
                tmp_sum = 0

        return max_sum

    def maxSubArray(self, nums: List[int]):
        return self.kadaneMaxSubArraySum(nums)


if __name__ == '__main__':
    print(Solution().maxSubArray([-2]))
