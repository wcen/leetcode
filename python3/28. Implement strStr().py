#!/usr/bin/env python3
"""
Q: 給定兩字串A, B，若B字串存在於A字串，則返回其索引。
Link: https://leetcode.com/problems/implement-strstr/
"""

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        haystack = haystack if haystack else ''
        needle = needle if needle else ''

        if len(needle) == 0: return 0

        if needle in haystack:
            for idx in range(0, len(haystack)):
                if haystack[idx:].startswith(needle):
                    return idx
        return -1

if __name__ == '__main__':
    print(Solution().strStr('', ''))
