#!/usr/bin/env python3
"""
Q: 陣列中皆為正整數，每一位數代表其10進位的值，將陣列表示的十進位值加上1後存回原本的陣列，並返回。
Link: https://leetcode.com/problems/plus-one/
"""

from typing import List

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        length = len(digits)
        add_one = None
        for idx in range(length):
            result = int()

            if idx == 0:
                result = digits[length - idx - 1] + 1

            if add_one is True:
                result = digits[length - idx - 1] + 1

            add_one = bool(result >= 10)

            if result:
                digits[length - idx - 1] = int(str(result)[-1])

        if add_one is True:
            digits.insert(0, 1)

        return digits

if __name__ == '__main__':
    print(Solution().plusOne([9, 9, 9]))
