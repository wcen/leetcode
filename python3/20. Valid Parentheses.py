#!/usr/bin/env python3
"""
Q: 判斷左右括號，數量、位置是否合理。
Link: https://leetcode.com/problems/valid-parentheses/
"""


class Solution:
    def isValid(self, s: str) -> bool:
        """ 遇到的右括號，一定要match最後遇到的左括號
        """
        if len(s) % 2 != 0: # 總括號數不是偶數
            return False

        _dict = {
            '(': ')',
            '[': ']',
            '{': '}'
        }

        left_brackets = []

        for item in s:
            if item in _dict.keys():
                left_brackets.append(item)

            elif item in _dict.values():
                if not left_brackets:   # 遇到有右括號，但是沒有左括號的時候
                    return False

                if item != _dict.get(left_brackets[-1]):    # 遇到右括號就去比對最後一個左括號
                    return False

                elif item == _dict.get(left_brackets[-1]):  # 比對成功就從列表拿掉
                    del left_brackets[-1]

                else:   # 不應有其他情形
                    return False

        if left_brackets:   # 如果還剩下左括號，表示左右個數不匹配
            return False

        else:
            return True


if __name__ == '__main__':
    print(Solution().isValid('){'))
