"""
Q: 給定特別的26字母順序，排序字串。
Link: https://leetcode.com/problems/verifying-an-alien-dictionary/
"""
from typing import List


class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:
        order_map = {}
        for idx, val in enumerate(order):
            order_map[val] = idx

        for i in range(len(words) - 1): # iterate words
            for j in range(len(words[i])):  # iterate characters of the word
                if j >= len(words[i + 1]):  # 僅在類似['apple', 'app']這種情形會成立
                    return False
                if words[i][j] != words[i + 1][j]:  # 字母不同時，才需要判斷order
                    if order_map[words[i][j]] > order_map[words[i + 1][j]]:
                        return False
                    break
        return True
