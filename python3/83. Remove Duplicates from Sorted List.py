"""
Q: 已排序的ListNode去重複。
Link: https://leetcode.com/problems/remove-duplicates-from-sorted-list/
"""
from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def deleteDuplicates(self, head: Optional[ListNode]) -> Optional[ListNode]:
        # visted用來存放不重複的值，若值存在於visited，表示該ListNode為重複的。
        visited = []
        current_node = head
        previous_node = None

        while current_node:
            value = current_node.val

            if value not in visited:
                visited.append(value)
                previous_node = current_node
            else:
                # 當下ListNode的值重複的話，
                # 將上一個（未重複）的next指到當下ListNode的next。
                previous_node.next = current_node.next

            current_node = current_node.next

        return head
