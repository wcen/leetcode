"""
Q: 合併兩個已排序的整數陣列。
Link: https://leetcode.com/problems/merge-sorted-array/
"""
from typing import List

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        # 表示目前要比較的值，從最後面開始比較（最大值開始比）
        nums1_idx = m - 1
        nums2_idx = n - 1

        # 迭代整個nums1長度
        for i in range(1, len(nums1) + 1):
            # nums2提早比較完，提早結束
            if nums2_idx < 0:
                break
            # 表示nums2還沒比較完，但當下比較的nums1已經沒了
            if nums1_idx < 0:
                nums1[-i] = nums2[nums2_idx]
                nums2_idx -= 1
                continue

            # -1最後一個、-2倒數第二個，依此類推。
            if nums1[nums1_idx] < nums2[nums2_idx]:
                nums1[-i] = nums2[nums2_idx]
                nums2_idx -= 1
            else:
                nums1[-i] = nums1[nums1_idx]
                nums1_idx -= 1
        print(nums1)
