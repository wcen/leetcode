#!/usr/bin/env python3
"""
Q: nums 中兩值若符合 target，返回其 index 陣列。
Link: https://leetcode.com/problems/two-sum/
"""

from typing import List

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i in range(0, len(nums)):
            search_num = target - nums[i]
            if search_num in nums:
                index = nums.index(search_num)
                if i != index:
                    return [i, index]

if __name__ == '__main__':
    print(Solution().twoSum([1, 2, 3], 5))
