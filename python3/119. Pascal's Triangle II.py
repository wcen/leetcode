#!/usr/bin/env python3
"""
Q: 帕斯卡三角形。
Link: https://leetcode.com/problems/pascals-triangle-ii/
"""

from typing import List


class Solution:
    def getRow(self, rowIndex: int) -> List[int]:
        _list = [1]

        while len(_list) - 1 != rowIndex:
            n_list = []
            for x in range(len(_list)):
                if x+1 == len(_list):
                    continue
                n_list.append(_list[x]+_list[x+1])
            n_list.insert(0, 1)
            n_list.append(1)

            _list = n_list

        return _list


if __name__ == '__main__':
    print(Solution().getRow(10))
