#!/usr/bin/env python3
"""
Q: 爬樓梯，每次一階或兩階，求排列組合。(費氏數列)
Link: https://leetcode.com/problems/climbing-stairs/
"""


class Solution:
    def climbStairs(self, n: int) -> int:
        """ 解釋
        最後一步可能為1或2，所以第n步的所有可能為第n-1步+第n-2步的所有可能
        f(n) = f(n-1) + f(n-2), n>2
        """
        if n <= 2:
            return n

        f = [0 ,1, 2]
        for i in range(3, n+1):
            # f[i] = f[i-1] + f[i-2]    # 錯誤示範，IndexError
            f.append(f[i-1] + f[i-2])

        return f[n]

if __name__ == '__main__':
    print(Solution().climbStairs(3))
