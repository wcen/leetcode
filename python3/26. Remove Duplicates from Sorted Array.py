#!/usr/bin/env python3
"""
Q: 給定一陣列，其中元素均為整數且可能重複，返回其不重複項目的長度。
Link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/
"""

from typing import List

class Solution:

    def removeDuplicates(self, nums: List[int]) -> int:
        """ 
        限制：
            - 給一 non-decreasing order 的整數陣列（類似遞增，但是整數可以重複）
        目標：
            - 不宣告額外記憶體O(1) space
            - 遍歷一次O(n) time
        """
        k = 1
        for idx in range(1, len(nums)):     # 從第二個開始看
            if nums[idx] != nums[idx-1]:    # 當下這個跟上一個不一樣則表示進位了
                nums[k] = nums[idx]
                k += 1
        return k

if __name__ == '__main__':
    nums = [0,0,1,1,1,2,2,3,3,4]
    Solution().removeDuplicates(nums)
