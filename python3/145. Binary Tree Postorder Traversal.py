"""
Q: 二元樹後序遍歷。
Link: https://leetcode.com/problems/binary-tree-postorder-traversal/
"""
from typing import Optional, List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def postorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        self.postorder(root, stack)
        return stack

    def postorder(self, node: Optional[TreeNode], stack: list):
        if node:
            self.postorder(node.left, stack)
            self.postorder(node.right, stack)
            stack.append(node.val)
        else:
            return
