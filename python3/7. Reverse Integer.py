#!/usr/bin/env python3
"""
Q : 給定一整數，返回反轉後的整數，若返回數超出範圍32-bit signed integer([−2^31,  2^31 − 1])則返回0。
Link: https://leetcode.com/problems/reverse-integer/
"""

class Solution:
    def reverse(self, x: int) -> int:
        reverse_str = ''
        negative = None
        for num in list(str(x)):
            if num == '-':
                negative = True
            else:
                reverse_str = num + reverse_str

        if negative is True:
            reverse_str = '-' + reverse_str

        if -2 ** 31 -1 < int(reverse_str) < 2 ** 31:
            return int(reverse_str)
        else:
            return 0

if __name__ == '__main__':
    print(Solution().reverse(-1234))
