"""
Q: 對稱二元樹。
Link: https://leetcode.com/problems/symmetric-tree/description/
"""
from typing import Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True
        return self.isSymmetricLeaf(root.left, root.right)

    def isSymmetricLeaf(self, left: Optional[TreeNode], right: Optional[TreeNode]):
        if not left and not right:
            return True
        if not left or not right:
            return False
        if left.val != right.val:
            return False

        return (self.isSymmetricLeaf(left.left, right.right) and
                self.isSymmetricLeaf(left.right, right.left))
