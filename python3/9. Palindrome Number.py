#!/usr/bin/env python3
"""
Q: 給一整數，若正向讀與反向讀，數值相同返回真，不同返回假。
Link: https://leetcode.com/problems/palindrome-number/
"""

class Solution:
    def isPalindrome(self, x: int) -> bool:
        """ 使用字串反轉
        """
        if str(x) == str(x)[::-1]:
            return True
        return False

if __name__ == '__main__':
    print(Solution().isPalindrome(121))
