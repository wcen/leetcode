#!/usr/bin/env python3
"""
Q: 有兩個ListNode，先反轉ListNode，並以number呈現，再相加兩個number，並以ListNode返回。
Link: https://leetcode.com/problems/add-two-numbers/
"""

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        def getListNodeSum(node: ListNode):
            """
            Input ListNode and Output the number
            e.g. [1,2,3] -> 321
            """
            num = 0
            idx = 0
            current_node = node
            while True:
                num += current_node.val * (10 ** idx)

                if current_node.next is None:
                    break

                idx += 1
                current_node = current_node.next
            return num

        l1_num = getListNodeSum(node=l1)
        l2_num = getListNodeSum(node=l2)

        sum = l1_num + l2_num
        sum_list = list()
        for i in reversed(list(str(sum))):
            sum_list.append(int(i))

        for idx, item in enumerate(sum_list):
            current_node = ListNode(val=item)

            if idx == 0:
                list_node = current_node
            else:
                previous_node.next = current_node

            previous_node = current_node

        return list_node


if __name__ == '__main__':
    print(
        Solution().addTwoNumbers(
            ListNode(val=1, next=ListNode(val=2, next=ListNode(val=3))),
            ListNode(val=2, next=ListNode(val=3, next=ListNode(val=4)))
            ).val
        )
