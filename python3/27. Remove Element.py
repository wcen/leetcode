#!/usr/bin/env python3
"""
Q: 將陣列內的元素，就地刪除，並返回原本的陣列。
Link: https://leetcode.com/problems/remove-element/
"""

from typing import List

class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        for num in reversed(nums):
            if val == num:
                nums.remove(num)
        return len(nums)

if __name__ == '__main__':
    print(Solution().removeElement([1, 2, 3], 2))
