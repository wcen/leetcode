#!/usr/bin/env python3
"""
Q: 求出平方根，取整數。
Link: https://leetcode.com/problems/sqrtx/
"""

class Solution:
    def mySqrt(self, x: int) -> int:
        return int(x ** 0.5)


if __name__ == '__main__':
    print(Solution().mySqrt(8))
