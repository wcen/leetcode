#!/usr/bin/env python3
"""
Q: 返回最大獲利金額。
Link: https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
"""
from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        """
        每天都算與前一天的獲利，再判斷獲利正/負，正累加，負重算。
        時間複雜度：O(N)
        """
        curr_profit = 0
        max_profit = 0

        for idx in range(1, len(prices)):
            curr_profit += prices[idx] - prices[idx - 1]

            if curr_profit < 0:
                curr_profit = 0

            if curr_profit > max_profit:
                max_profit = curr_profit

        return max_profit


if __name__ == '__main__':
    print(Solution().maxProfit([7, 9, 2, 6]))
