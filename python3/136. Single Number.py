#!/usr/bin/env python3
"""
Q: 返回單數的整數。
Link: https://leetcode.com/problems/single-number/
"""

from typing import List

class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        for idx in range(0, len(nums)):
            # 去掉當前的item，再搜尋
            if not nums[idx] in nums[0:idx] + nums[idx+1: len(nums)+1]:
                return nums[idx]

        return nums[0]


if __name__ == '__main__':
    print(Solution().singleNumber([1]))
