"""
Q: 二元樹中序遍歷。
Link: https://leetcode.com/problems/binary-tree-inorder-traversal/
"""
from typing import List, Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        self.inorder(root, stack)
        return stack

    # 遞迴解（Recursion）
    def inorder(self, node: Optional[TreeNode], stack: list):
        # Python中傳遞dict/list，傳址非傳值，所以修改的全為同一個dict/list
        # 按中序遍歷，左根右，把val存下來
        if node:
            self.inorder(node.left, stack)
            stack.append(node.val)
            self.inorder(node.right, stack)
        else:
            return
