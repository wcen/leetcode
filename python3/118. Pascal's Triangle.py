#!/usr/bin/env python3
"""
Q: 帕斯卡三角形。
Link: https://leetcode.com/problems/pascals-triangle/
"""

from typing import List


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        _list = [[1]]

        for x in range(numRows):
            try:
                _list[x]

            except IndexError:
                _list.append([])

                for y in range(len(_list[x-1])):
                    if y+1 == len(_list[x-1]):
                        continue
                    _list[x].append(_list[x-1][y]+_list[x-1][y+1])

                _list[x].insert(0, 1)
                _list[x].append(1)

        return _list


if __name__ == '__main__':
    print(Solution().generate(5))
