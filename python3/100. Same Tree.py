"""
Q: 比較兩二元樹，是否相同
Link: https://leetcode.com/problems/same-tree/
"""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        # 先排除掉p或q可能為空的情況
        if not p and q:
            return False
        if p and not q:
            return False
        if not p and not q:
            return True

        # 其餘情況表示p或q一定是TreeNode，
        # 則只要判斷TreeNode的val。
        if p.val != q.val:
            return False
        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)
