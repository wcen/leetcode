"""
Q: 返回重複的字串（最長的）部分。
Link: https://leetcode.com/problems/greatest-common-divisor-of-strings/
"""
class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        tmp_str = ''

        for i in range(len(str2), 0, -1):
            prefix = str2[0:i]
            # 不能整除，表示str2長度與prefix長度不匹配
            if len(str2) % len(prefix) != 0:
                continue
            # 可以整除，但是不是全由prefix組成的字串
            if str2 != prefix * int(len(str2)/len(prefix)):
                continue

            if str1.startswith(prefix):
                # 同上，不能整除，表示str1長度與prefix長度不匹配
                if len(str1) % len(prefix) != 0:
                    continue
                # prefix要重複幾次
                times = int(len(str1)/len(prefix))
                if times > 0 and str1 == prefix * times:
                    # 留長的
                    if len(tmp_str) < len(prefix):
                        tmp_str = prefix

        return tmp_str
